import matplotlib.pyplot as plt
import numpy as np
from FREClasses import FRE, GREVesselMagPools, GREMagPool
from FREplotting import plot_fre_voxel, plot_fre_heatmap

Voxel1 = FRE(tr=20, te=7, fa=20, n_dummies=1000, voxel_length_mm=0.25,
             t1_blood=2000, t2s_blood=60, vessel_diameter_mm=0.25, vessel_in_voxel_fraction=1.0,
             blood_delivery_time_s=0.2, blood_velocity_mm_s=1.0,
             t1_tissue=1600, t2s_tissue=40)
RelFRE, VolWeightedFRE, VoxelMag, VolWeightedVoxelMag = Voxel1.calc_fre()
print("---FRE---")
print(Voxel1.NPriorPulses)
print(RelFRE, VolWeightedFRE, VoxelMag, VolWeightedVoxelMag)
print("---------")

# # # # #
Voxel1.BloodDeliveryTime = 0.0
Voxel1.BloodVelocity = 10.0

voxel_lens = np.arange(0.1, 1.1, 0.1)
vessel_diameters = np.arange(0.1, 0.6, 0.1)
rel_fre = np.zeros([len(vessel_diameters), len(voxel_lens)])
vw_rel_fre = np.zeros([len(vessel_diameters), len(voxel_lens)])
voxel_mag = np.zeros([len(vessel_diameters), len(voxel_lens)])
vw_voxel_mag = np.zeros([len(vessel_diameters), len(voxel_lens)])
for j in range(0, len(voxel_lens)):
    for i in range(0, len(vessel_diameters)):
        Voxel1.VoxelLen = voxel_lens[j]
        Voxel1.VesselDiameter = vessel_diameters[i]
        rel_fre[i, j], vw_rel_fre[i, j], voxel_mag[i, j], vw_voxel_mag[i, j] = Voxel1.calc_fre()

plot_fre_heatmap(rel_fre, voxel_lens, vessel_diameters, 'relative FRE')
plot_fre_heatmap(vw_rel_fre, voxel_lens, vessel_diameters, 'vw-relative FRE')
plot_fre_heatmap(voxel_mag, voxel_lens, vessel_diameters, 'Voxel Mag')
plot_fre_heatmap(vw_voxel_mag, voxel_lens, vessel_diameters, 'vw-Voxel Mag')


# # # # #
Voxel1.BloodDeliveryTime = 0.0
Voxel1.BloodVelocity = 10.0
Voxel1.VesselDiameter = 0.3

voxel_lens = np.arange(0.1, 0.6, 0.1)
vessel_fraction = np.arange(0.1, 1.1, 0.1)
rel_fre = np.zeros([len(vessel_fraction), len(voxel_lens)])
voxel_mag = np.zeros([len(vessel_fraction), len(voxel_lens)])
for j in range(0, len(voxel_lens)):
    for i in range(0, len(vessel_fraction)):
        Voxel1.VoxelLen = voxel_lens[j]
        Voxel1.VesselInVoxelFraction = vessel_fraction[i]
        rel_fre[i, j], _, voxel_mag[i, j], _ = Voxel1.calc_fre()

plot_fre_heatmap(rel_fre, voxel_lens, vessel_fraction, 'relative FRE')
plot_fre_heatmap(voxel_mag, voxel_lens, vessel_fraction, 'Voxel Mag')


# # # # #
Voxel1.VoxelLen = 0.25
Voxel1.VesselDiameter = 0.25
Voxel1.VesselInVoxelFraction = 1.0
velocities = np.arange(0.5, 10.5, 0.5)
delivery_times = np.arange(0.0, 1.1, 0.1)
rel_fre = np.zeros([len(delivery_times), len(velocities)])
for j in range(0, len(velocities)):
    for i in range(0, len(delivery_times)):
        Voxel1.BloodVelocity = velocities[j]
        Voxel1.BloodDeliveryTime = delivery_times[i]
        rel_fre[i, j], _, _, _ = Voxel1.calc_fre()

plot_fre_heatmap(rel_fre, velocities, delivery_times, 'relative FRE')


# # # # #
plot_fre_voxel(Voxel1)

# # # # #
Vessel1 = GREVesselMagPools(tr=20, te=3, t1=2200, t2s=50, fa=25, n_prior_pulses=10,
                            vessel_len_mm=0.1, blood_velocity_mm_s=1.0)
print(Vessel1.NPools)
print(Vessel1.SegLen)

WeightedMag, MagPerPool = Vessel1.calc_weighted_vessel_mag()
print(WeightedMag)
print(MagPerPool)

Mag1 = GREMagPool(n=10, tr=20, t1=2000, t2s=10, fa=45)
Mz, Mxy = Mag1.calc_mag_evolution_over_n_pulses(m=200)
ax1 = plt.subplot(2, 1, 1)
plt.plot(Mz)
ax2 = plt.subplot(2, 1, 2)
plt.plot(Mxy)
plt.show()
