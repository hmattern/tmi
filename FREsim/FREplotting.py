from matplotlib.patches import Rectangle
import matplotlib.pyplot as plt
import numpy as np


def plot_fre_voxel(fre, size_pixel=None):
    # init
    size = size_pixel if size_pixel is not None else 256

    # compute scaling factor to convert from mm to pixel
    scaling = size/fre.VoxelLen

    # create figure
    fig = plt.figure()
    ax = fig.add_subplot(111)

    # plot voxel as rectangle with tissue intensity
    rect_voxel = Rectangle((0, 0), size, size, color=(fre.MagTissue, fre.MagTissue, fre.MagTissue))
    ax.add_patch(rect_voxel)

    # plot vessel segments
    # first set y position and size aka height
    vessel_height = np.round(scaling*fre.VesselDiameter)
    vessel_pos_y = np.round(size/2 - vessel_height/2)
    for i in range(0, len(fre.SegLen)):
        # for each segment find x position and size aka width
        vessel_width = scaling * fre.SegLen[i]
        vessel_pos_x = scaling * fre.SegLen[0] * i  # use the first segment, otherwise problems for the "remainder" seg

        # plot the segment
        rect_segment = Rectangle((vessel_pos_x, vessel_pos_y), vessel_width, vessel_height,
                                 color=(fre.MagsBlood[i], fre.MagsBlood[i], fre.MagsBlood[i]))
        ax.add_patch(rect_segment)

    plt.xlim([0, size])
    plt.ylim([0, size])
    plt.show()

    # todo: volume or diameter adjusted plottng; scalling of voxel intensities from [min max] or [0 1]; voxel fraction thingy


def plot_fre_heatmap(data, xticks, yticks, plot_title, label_x_axis=None, label_y_axis=None, c_map_string=None,
                     plot_values=None):
    # init figure
    label_x_axis = label_x_axis if not None else ''
    label_y_axis = label_y_axis if not None else ''
    c_map_string = c_map_string if not None else 'gray'
    plot_values = plot_values if not None else False

    # plot
    fig, ax = plt.subplots()
    im = ax.imshow(data, cmap=c_map_string)

    # plot values if required
    if plot_values:
        for i in range(len(yticks)):
            for j in range(len(xticks)):
                text = ax.text(j, i, np.around(data[i, j], decimals=2),
                               ha="center", va="center", color="w")

    # set ticks
    # we want to show all ticks...
    ax.set_xticks(np.arange(len(xticks)))
    ax.set_yticks(np.arange(len(yticks)))
    # ... and label them with the respective list entries; FYI truncate tick labels
    ax.set_xticklabels(np.around(xticks, decimals=2))
    ax.set_yticklabels(np.around(yticks, decimals=2))

    # rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # color bar, labels, etc.
    fig.colorbar(im)
    ax.set_title(plot_title)
    plt.xlabel(label_x_axis)
    plt.ylabel(label_y_axis)
    fig.tight_layout()
    plt.show()
