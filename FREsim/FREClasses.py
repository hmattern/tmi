import numpy as np


class GREMagPool:
    """this is the magnetization pool base class"""

    def __init__(self, tr=None, te=None, t1=None, t2s=None, mz_start=None, fa=None, n=None):
        # set variables or use defaults
        self.TR = tr if tr is not None else 20
        self.TE = te if te is not None else 6
        self.T1 = t1 if t1 is not None else 2000
        self.T2s = t2s if t2s is not None else 20
        self.MzStart = mz_start if mz_start is not None else 1.0
        self.FA = fa if fa is not None else 10
        self.N = n if n is not None else 100

        # checks for TE & TR
        if self.TE > self.TR:
            self.TE = self.TR
        if self.TR == 0:
            self.TR = 1

        # define "output" variables
        self.MzCurrent = self.MzStart
        self.MxyCurrent = self.__calc_mxy(self.TE)

    def __calc_mz(self, t=None, mz=None):
        if t is None:
            t = self.TR
        if mz is None:
            mz = self.MzCurrent
        e1 = np.exp(-t/self.T1)
        return mz * np.cos(np.deg2rad(self.FA)) * e1 + 1 - e1

    def __calc_mxy(self, t=None, mz=None):
        if t is None:
            t = self.TE
        if mz is None:
            mz = self.MzCurrent
        e2s = np.exp(-t/self.T2s)
        return mz * np.sin((np.deg2rad(self.FA))) * e2s

    def calc_mag_after_n_pulses(self):
        # returns for longitudinal magnetization the state prior to each pulse/before excitation
        # returns for transversal magnetization the state at TE
        mz = np.zeros(self.N)
        mz[0] = self.MzStart
        for i in range(1, self.N):
            mz[i] = self.__calc_mz(self.TR, mz[i - 1])
        self.MzCurrent = mz[-1]
        self.MxyCurrent = self.__calc_mxy(t=self.TE)
        return mz

    def calc_mz_with_tr(self, m=None, mz_start=None):
        # m = number of computed data points
        if m is None:
            m = 1000
        if mz_start is None:
            mz_start = self.MzCurrent
        mz = np.array([])
        for t in np.arange(0, self.TR, self.TR/m):
            mz = np.append(mz, self.__calc_mz(t, mz_start))
        return mz

    def calc_mxy_with_tr(self, m=None, mz_start=None):
        # m = number of computed data points
        if m is None:
            m = 1000
        if mz_start is None:
            mz_start = self.MzCurrent
        mxy = np.array([])
        for t in np.arange(0, self.TR, self.TR/m):
            mxy = np.append(mxy, self.__calc_mxy(t, mz_start))
        return mxy

    def calc_mag_evolution_over_n_pulses(self, m=None):
        # m = number of computed points with in each TR
        if m is None:
            m = 100
        # init output arrays
        mz = np.array([])
        mxy = np.array([])
        # compute mz before each excitation
        mz_per_tr = self.calc_mag_after_n_pulses()
        # compute mz and m_xy within each TR
        for mz_current_tr in mz_per_tr:
            mz = np.append(mz, self.calc_mz_with_tr(m, mz_start=mz_current_tr))
            mxy = np.append(mxy, self.calc_mxy_with_tr(m, mz_start=mz_current_tr))
        return mz, mxy


class GREVesselMagPools:
    """this class simulates the different mag pools/signals a vessels has due to different inflow enhancement"""
    def __init__(self, tr=None, te=None, t1=None, t2s=None, fa=None, n_prior_pulses=None,
                 vessel_len_mm=None, blood_velocity_mm_s=None):
        # set variables or use defaults
        self.TR = tr if tr is not None else 20
        self.TE = te if te is not None else 6
        self.T1 = t1 if t1 is not None else 2000
        self.T2s = t2s if t2s is not None else 20
        self.FA = fa if fa is not None else 10
        # number of pulses the blood experienced before entering the voxel of interest (aka pulses during delivery time)
        self.NPriorPulses = n_prior_pulses if n_prior_pulses is not None else 10
        # vessel length in mm
        self.VesselLen = vessel_len_mm if vessel_len_mm is not None else 1
        # blood velocity in mm/s
        self.BloodVelocity = blood_velocity_mm_s if blood_velocity_mm_s is not None else 1

        # compute the number of blood vessel magnetization pools
        self.NPools = self.__calc_number_of_pools()
        # compute vessel segment lengths
        self.SegLen = self.__calc_vessel_segment_lengths()

    def __calc_number_of_pools(self):
        # compute the number of blood vessel magnetization pools
        # FYI: convert TR from ms to s
        n_float = self.VesselLen / (self.BloodVelocity * self.TR * 0.001)
        # if an integer number of pools/segments was found we are lucky
        if n_float % 1.0 == 0.0:
            n = int(n_float)
        else:
            # else, there are n pools of length <Velocity * TR> plus 1 pool of the length VesselLen - LengthFullSegments
            n = int(np.floor(n_float)) + 1
        return n

    def __calc_vessel_segment_lengths(self):
        # FYI: convert TR from ms to s
        full_seg_len = self.BloodVelocity * self.TR * 0.001
        # single segment cannot be longer than vessel itself
        if full_seg_len > self.VesselLen:
            full_seg_len = self.VesselLen
        # init array with all the lengths
        seg_len = np.zeros(self.NPools)
        # trivial case: 1 segment only
        if full_seg_len == self.VesselLen:
            seg_len[0] = full_seg_len
        else:
            # the first [0] to second to last segment [NPools-1] have the full segment length
            for i in range(0, self.NPools-1):
                seg_len[i] = full_seg_len
            # the last segment [NPools-2] has the remaining length of <VesselLen-(NPools-1)*FullSegLen>
            seg_len[-1] = self.VesselLen - (self.NPools-1)*full_seg_len

        return seg_len

    def calc_weighted_vessel_mag(self):
        # compute the number of blood vessel magnetization pools
        self.NPools = self.__calc_number_of_pools()
        # compute vessel segment length
        self.SegLen = self.__calc_vessel_segment_lengths()
        # compute the transversal mag for each vessel segment / pool
        # first set up a magnetization pool instance with the proper parameters
        mag_pool = GREMagPool(tr=self.TR, te=self.TE, t1=self.T1, t2s=self.T2s, mz_start=1.0, fa=self.FA, n=0)
        # now compute the magnetization per pool
        # each pool has seen NPriorPulses plus pulses within the voxel
        # the number of pulses within the voxel range from 1 (first segment) to NPools (last segment)
        mag_per_pool = np.zeros([self.NPools])
        for i in range(0, self.NPools):  # hm: this loop could be unrolled if MxyHistory would be returned by GREMagPool
            # set number of all pulses experienced by this blood pool (<+1> to account for very first excitation)
            mag_pool.N = self.NPriorPulses + i + 1
            # calc magnetization
            mag_pool.calc_mag_after_n_pulses()
            # copy Mxy results
            mag_per_pool[i] = mag_pool.MxyCurrent
        # last, weight the magnetization per pool/segment with its relative length (proportional to volume)
        weighted_mag = np.sum(np.multiply(mag_per_pool, self.SegLen)/self.VesselLen)
        return weighted_mag, mag_per_pool


class FRE:
    """this class simulates the flow-related enhancement"""
    def __init__(self, tr=None, te=None, fa=None, n_dummies=None, voxel_length_mm=None,
                 t1_blood=None, t2s_blood=None, vessel_diameter_mm=None, blood_delivery_time_s=None,
                 vessel_in_voxel_fraction=None, blood_velocity_mm_s=None,
                 t1_tissue=None, t2s_tissue=None):
        # set variables or use defaults
        # sequence parameters: TR, TE, flip angle (FA), number of dummy shots (n_dummies)
        self.TR = tr if tr is not None else 20
        self.TE = te if te is not None else 4
        self.FA = fa if fa is not None else 18
        self.NDummies = n_dummies if n_dummies is not None else 100

        # voxel parameters: voxel volume = (voxel length)^3 --> isotropic voxel assumed
        self.VoxelLen = voxel_length_mm if voxel_length_mm is not None else 1.0  # in mm
        if self.VoxelLen == 0:
            self.VoxelLen = 0.001  # in mm --> 1 um

        # blood parameters
        self.T1Blood = t1_blood if t1_blood is not None else 2100  # in ms
        self.T2sBlood = t2s_blood if t2s_blood is not None else 62  # in ms
        self.VesselDiameter = vessel_diameter_mm if vessel_diameter_mm is not None else 0.25  # in mm
        # time the blood spends in the slab before entering the voxel of interest
        self.BloodDeliveryTime = blood_delivery_time_s if blood_delivery_time_s is not None else 1.0  # in s
        # fraction of the vessel within the voxel; account for vessels only partially inside voxel
        self.VesselInVoxelFraction = vessel_in_voxel_fraction if vessel_in_voxel_fraction is not None else 1.0  # [0..1]
        # blood velocity within the voxel
        self.BloodVelocity = blood_velocity_mm_s if blood_velocity_mm_s is not None else 1.0  # in mm/s

        # tissue parameters
        self.T1Tissue = t1_tissue if t1_tissue is not None else 1250  # in ms
        self.T2sTissue = t2s_tissue if t2s_tissue is not None else 40  # in ms

        # set sizes and compute volumes
        self.VesselLen = self.VoxelLen  # assume orthogonal vessel through voxel with same length
        self.VoxelVol = self.__calc_voxel_volume()
        self.VesselVol = self.__calc_vessel_volume()
        self.TissueVol = self.__calc_tissue_volume()

        # calculate the number of pulses experienced by the blood during the blood delivery time;
        # thus, the time the blood spend in the slab before entering the voxel of interest
        self.NPriorPulses = self.__calc_num_prior_pulses()

        # check the number of dummies:
        # the tissue has experienced at least NPriorPulses
        if self.NDummies < self.NPriorPulses:
            self.NDummies = self.NPriorPulses
        # or at least one pulse in case of full in-flow replacement per TR aka NPriorPulses = 0
        if self.NDummies == 0:
            self.NDummies = 1

        # init magnetization of tissue and blood pools; these are used ie for plotting
        self.MagTissue = 0.0
        self.MagAvgBlood = 0.0
        self.MagsBlood = np.array([])

        # store blood segment lengths; also required for plots
        self.SegLen = np.array([])

    def __calc_voxel_volume(self):
        return np.power(self.VesselLen, 3)

    def __calc_vessel_volume(self):
        # volume of the vessel cylinder
        vessel_vol = np.pi * np.power(self.VesselDiameter/2, 2) * self.VesselDiameter
        # vessel volume should NOT exceed voxel volume
        if vessel_vol > self.VoxelVol:
            vessel_vol = self.VoxelVol
        # now account for vessel fraction factor
        vessel_vol = vessel_vol * self.VesselInVoxelFraction
        return vessel_vol

    def __calc_tissue_volume(self):
        return self.VoxelVol - self.VesselVol

    def __calc_num_prior_pulses(self):
        # number of pulses blood experienced before entering the voxel is DeliveryTime/TR
        # FYI: convert TR from ms to s
        return int(np.floor(self.BloodDeliveryTime/(0.001*self.TR)))

    def calc_fre(self):
        # update volumes and lengths
        self.VesselLen = self.VoxelLen  # assume orthogonal vessel through voxel with same length
        self.VoxelVol = self.__calc_voxel_volume()
        self.VesselVol = self.__calc_vessel_volume()
        self.TissueVol = self.__calc_tissue_volume()

        # update number of pulses blood experienced before entering the voxel of interest
        self.NPriorPulses = self.__calc_num_prior_pulses()

        # calc tissue magnetization
        tissue_pool = GREMagPool(self.TR, self.TE, self.T1Tissue, self.T2sTissue, mz_start=1.0, fa=self.FA,
                                 n=self.NDummies)
        tissue_pool.calc_mag_after_n_pulses()
        self.MagTissue = tissue_pool.MxyCurrent

        # calc vessel magnetization
        vessel_pool = GREVesselMagPools(self.TR, self.TE, self.T1Blood, self.T2sBlood, self.FA,
                                        n_prior_pulses=self.NPriorPulses, blood_velocity_mm_s=self.BloodVelocity,
                                        vessel_len_mm=self.VesselLen)
        self.MagAvgBlood, self.MagsBlood = vessel_pool.calc_weighted_vessel_mag()
        self.SegLen = vessel_pool.SegLen

        # calc relative FRE
        rel_fre = self.MagAvgBlood/self.MagTissue

        # calc voxel magnetization
        voxel_mag = (self.MagAvgBlood * self.VesselVol + self.MagTissue * self.TissueVol) / self.VoxelVol

        # calc volume-weighted FRE = (VesselMag * VesselVol + TissueMag * TissueVol)/TissueMag
        # ... = voxel_mag /TissueMag
        vol_weighted_fre = voxel_mag / self.MagTissue

        # calc volume-weighted voxel magnetization = (VoxelMag * VoxelVolume) / 1mm^3
        vol_weighted_voxel_mag = voxel_mag * self.VoxelVol

        return rel_fre, vol_weighted_fre, voxel_mag, vol_weighted_voxel_mag
