import numpy as np
from FREClasses import FRE
from FREplotting import plot_fre_heatmap

Voxel1 = FRE(tr=20, te=7, fa=20, n_dummies=1000, voxel_length_mm=0.25,
             t1_blood=2000, t2s_blood=60, vessel_diameter_mm=0.5, vessel_in_voxel_fraction=1.0,
             blood_delivery_time_s=0.2, blood_velocity_mm_s=1.0,
             t1_tissue=1600, t2s_tissue=40)
_, VolWeightedFRE, VoxelMag, _ = Voxel1.calc_fre()

voxel_lens = np.arange(0.5, 0.0, -0.1)
velocity = np.arange(1, 21, 1)
vw_fre = np.zeros([len(velocity), len(voxel_lens)])
num_pools = np.zeros([len(velocity), len(voxel_lens)])
for j in range(0, len(voxel_lens)):
    for i in range(0, len(velocity)):
        Voxel1.VoxelLen = voxel_lens[j]
        Voxel1.BloodVelocity = velocity[i]
        _, vw_fre[i, j], _, _ = Voxel1.calc_fre()
        num_pools[i, j] = len(Voxel1.SegLen)

num_pools = num_pools.transpose()
vw_fre = vw_fre.transpose()


plot_fre_heatmap(vw_fre, velocity, voxel_lens,
                 'relative FRE with vessel diameter of ' + str(Voxel1.VesselDiameter) + ' mm',
                 label_y_axis='voxel length [mm]', label_x_axis='blood velocity [mm/s]', c_map_string='jet',
                 plot_values=True)

plot_fre_heatmap(num_pools, velocity, voxel_lens, 'number of (intra-voxel) blood pools',
                 label_y_axis='voxel length [mm]', label_x_axis='blood velocity [mm/s]', c_map_string='jet',
                 plot_values=True)
