Here we provide the CAD model (as STEP file) of our remotely controllable rotation device. This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.


If you use the data please cite:

Odenbach R, Thoma N, Mattern H, Friebe M. Remotely controllable phantom rotation system for ultra-high field MRI to improve Cross Calibration. Current Directions in Biomedical Engineering [Internet]. 2019;5(1):429–431 doi: 10.1515/cdbme-2019-1570538325 Titel anhand dieser DOI in Citavi-Projekt übernehmen.