clear; close all; clc;

%% init
% add nii handling
addpath('/home/hmattern/MATLAB/3rdParty/NIfTI_20140122/');
PathImg = './02_CoReg/';
PathDeMasked = './04_Demasked/';
PathSave = './05_Output/';

BWsT2s = {'100', '200', '300', '390', '980'};


%% load data T2s
ImgsT2s = zeros(224, 256, 96, length(BWsT2s));
ImgsT2sDM = ImgsT2s;

% load masked data
for i=1:length(BWsT2s)
    Data = load_untouch_nii([PathImg, 'greT2s1mmiPat2BW', BWsT2s{i}, '_N4_Warped.nii.gz']);
    ImgsT2s(:,:,:,i) = Data.img;    
end

% load demasked data
for i=1:length(BWsT2s)
    Data = load_untouch_nii([PathDeMasked, 'T2sBW', BWsT2s{i}, '_DeMasked.nii']);
    ImgsT2sDM(:,:,:,i) = Data.img;    

end


%% plot profiles for T2s SAG (only Oil)

% for cropping out ROI/slc
cMin = 0; cMax = 750;
yMin = 128; yMax = 256;
zMin = 5; zMax = 90;

SlcSag = 120;
% for profiles
xStart = 32; xEnd = xStart;
yStart = 90; yEnd = 115;

figure(1);
for i=size(ImgsT2s,4):-1:1
    % extract slice
    Slc = rot90(squeeze(ImgsT2s(SlcSag,yMin:yMax,zMin:zMax,i)));    
    SlcDM = rot90(squeeze(ImgsT2sDM(SlcSag,yMin:yMax,zMin:zMax,i)));    
          
    % extract profiles
    Profile = Slc(xStart:xEnd, yStart:yEnd);   
    ProfileDM = SlcDM(xStart:xEnd, yStart:yEnd);   
        
    % plot profile
    subplot(3,5,15-(i-1)); plot(1:length(Profile), Profile','-r', 'LineWidth',2.5);
    
    % plot profile (demasked)
    hold on; plot(1:length(ProfileDM), ProfileDM','--c', 'LineWidth',2.5);
    h=gca;
    h.XLim = ([-2, length(yStart:yEnd)+2]);
    h.YLim = ([-50, 1500]);
    h.TickLength = [0,0];
    
    % plot slice
    Slc(xStart:xEnd, yStart:yEnd) = cMax;
    subplot(3,5,5-(i-1)); imshow(Slc,[cMin, cMax]); 
        
    % plot slice (demasked)
    SlcDM(xStart:xEnd, yStart:yEnd) = cMax;
    subplot(3,5,10-(i-1)); imshow(SlcDM,[cMin, cMax]); 
  
end
pause(3);

%% save individual slice profiles for publication
close all;
figure(2); 
for i=size(ImgsT2s,4):-1:1
    % extract slice
    Slc = rot90(squeeze(ImgsT2s(SlcSag,yMin:yMax,zMin:zMax,i)));    
    SlcDM = rot90(squeeze(ImgsT2sDM(SlcSag,yMin:yMax,zMin:zMax,i)));    
    
    % extract profiles
    Profile = Slc(xStart:xEnd, yStart:yEnd);   
    ProfileDM = SlcDM(xStart:xEnd, yStart:yEnd);   
    
    % plot profile
    plot(1:length(ProfileDM), ProfileDM','-c', 'LineWidth',10);
    hold on;
    plot(1:length(Profile), Profile','-r', 'LineWidth',7.5);
    hold off;
    
    % adjust axis etc.
    h=gca;
    h.XLim = ([-10, length(yStart:yEnd)+10]);
    h.YLim = ([-50, 1500]);
    h.TickLength = [0,0];
        % to enable black background
    h.Color = [0 ,0, 0];
    set(gcf,'inverthardcopy','off'); 
        % grid
    h.XGrid = 'Off'; h.YGrid = 'On';
    h.GridColor = [1 1 1];
    h.YTick = 0:250:1500;
    h.LineWidth = 5;
    % add legend of BW = 100
    if i == 1       
        legend({'masked', 'unmasked'},'FontSize',18,'FontWeight','bold',...
            'TextColor', 'white');
        legend('boxoff');
        legend off
    end
    
    % set filename
    Fn = [PathSave, 'Profile_T2sBW',BWsT2s{i},'.tiff'];
    
    % save
    set(gca(), 'LooseInset', get(gca(), 'TightInset'));
    print(Fn,'-dtiffn',['-r0']);   
    
end
