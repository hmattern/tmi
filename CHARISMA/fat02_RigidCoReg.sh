#!/bin/bash

# Folders
InPath=${PWD}/01_N4/
OutPath=${PWD}/02_CoReg/

# list of all T2s Vol with Mask
declare -a VolT2s=("greT2s1mmiPat2BW100_N4" 
	 "greT2s1mmiPat2BW200_N4"
	 "greT2s1mmiPat2BW300_N4"
	 "greT2s1mmiPat2BW390_N4"
	 "greT2s1mmiPat2BW980_N4")


# Define Reference Volume
declare REF="${InPath}grePD1mmiPat2BW980noFlowCompNoMask_N4.nii.gz"

# Define Registration
declare REG="r"
declare THREADS="8"

# CoReg T2s with Mask
for i in "${VolT2s[@]}"
do

	echo
	echo ${i}

	${ANTSPATH}/antsRegistrationSyN.sh \
	  -d 3 \
	  -f "$REF" \
	  -m ${InPath}/${i}.nii.gz \
	  -o ${OutPath}/${i}_ \
	  -n "$THREADS" \
	  -t "$REG"

done

# T2s without Mask
${ANTSPATH}/antsRegistrationSyN.sh \
  -d 3 \
  -f "$REF" \
  -m ${InPath}/greT2s1mmiPat2BW100NoMask_N4.nii.gz \
  -o ${OutPath}/greT2s1mmiPat2BW100NoMask_N4_ \
  -n "$THREADS" \
  -t "$REG"


# T1 200
${ANTSPATH}/antsRegistrationSyN.sh \
  -d 3 \
  -f "$REF" \
  -m ${InPath}/grePD1mmiPat2BW200noFlowComp_N4.nii.gz \
  -o ${OutPath}/grePD1mmiPat2BW200noFlowComp_N4_ \
  -n "$THREADS" \
  -t "$REG"

# T1 980
${ANTSPATH}/antsRegistrationSyN.sh \
  -d 3 \
  -f "$REF" \
  -m ${InPath}/grePD1mmiPat2BW980noFlowComp_N4.nii.gz \
  -o ${OutPath}/grePD1mmiPat2BW980noFlowComp_N4_ \
  -n "$THREADS" \
  -t "$REG"

# T1 980 without Mask
${ANTSPATH}/antsRegistrationSyN.sh \
  -d 3 \
  -f "$REF" \
  -m ${InPath}/grePD1mmiPat2BW980noFlowCompNoMask_N4.nii.gz \
  -o ${OutPath}/grePD1mmiPat2BW980noFlowCompNoMask_N4_ \
  -n "$THREADS" \
  -t "$REG"
