#!/bin/bash

# Folders
PathCoReg=${PWD}/02_CoReg/
PathDeMasked=${PWD}/04_Demasked/
PathOut=${PWD}/05_Output/

# Options
declare Opt=":grayscale=0,500 -slice 1 1 58 -viewport axial"
#declare Opt=":grayscale=0,500 -viewport sagittal"

# list of all Vols (Co-Reg)
declare -a Vols=("grePD1mmiPat2BW980noFlowCompNoMask_N4_Warped"
				"grePD1mmiPat2BW980noFlowComp_N4_Warped"
				"grePD1mmiPat2BW200noFlowComp_N4_Warped")

# Create and Save Screenshots (Rigid)
for i in "${Vols[@]}"
do
	freeview ${PathCoReg}${i}.nii.gz${Opt}  -ss ${PathOut}${i}.png 1
done

# list of all Vols (DeMasked)
declare -a Vols=("PDBW980_DeMasked"
				"PDBW200_DeMasked")

# Create and Save Screenshots (DeMasked)
for i in "${Vols[@]}"
do
	freeview ${PathDeMasked}${i}.nii${Opt}  -ss ${PathOut}${i}.png 1
done