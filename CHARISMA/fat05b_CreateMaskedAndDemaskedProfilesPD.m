clear; close all; clc;

%% init
% add nii handling
addpath('/home/hmattern/MATLAB/3rdParty/NIfTI_20140122/');
PathImg = './02_CoReg/';
PathDeMasked = './04_Demasked/';
PathSave = './05_Output/';

BWsPD = {'200', '980'};


%% load data 
ImgsPD = zeros(224, 256, 96, length(BWsPD));
ImgsPDDM = ImgsPD;

% load masked data
for i=1:length(BWsPD)
    Data = load_untouch_nii([PathImg, 'grePD1mmiPat2BW', BWsPD{i}, 'noFlowComp_N4_Warped.nii.gz']);
    ImgsPD(:,:,:,i) = Data.img;    
end

% load demasked data
for i=1:length(BWsPD)
    Data = load_untouch_nii([PathDeMasked, 'PDBW', BWsPD{i}, '_DeMasked.nii']);
    ImgsPDDM(:,:,:,i) = Data.img;    

end


%% plot profiles for PD AX

% for cropping out ROI/slc
cMin = 0; cMax = 750;
xMin = 1; xMax = 224;
yMin = 128; yMax = 256;

TRASlc = 58;
% for profiles
xStart = 16; xEnd = 42;
yStart = 97; yEnd = yStart;
close
figure(1);
for i=size(ImgsPD,4):-1:1
    % extract slice
    Slc = rot90(ImgsPD(xMin:xMax,yMin:yMax,TRASlc,i));    
    SlcDM = rot90(ImgsPDDM(xMin:xMax,yMin:yMax,TRASlc,i));    
          
    % extract profiles
    Profile = Slc(xStart:xEnd, yStart:yEnd);   
    ProfileDM = SlcDM(xStart:xEnd, yStart:yEnd);   
        
    % plot profile
    subplot(3,2,6-(i-1)); plot(1:length(Profile), Profile','-r', 'LineWidth',2.5);
    
    % plot profile (demasked)
    hold on; plot(1:length(ProfileDM), ProfileDM','--c', 'LineWidth',2.5);
    h=gca;
    h.XLim = ([-2, length(xStart:xEnd)+2]);
    h.YLim = ([-50, 450]);
    h.TickLength = [0,0];
    
    % plot slice
    Slc(xStart:xEnd, yStart:yEnd) = cMax;
    subplot(3,2,2-(i-1)); imshow(Slc,[cMin, cMax]); 
        
    % plot slice (demasked)
    SlcDM(xStart:xEnd, yStart:yEnd) = cMax;
    subplot(3,2,4-(i-1)); imshow(SlcDM,[cMin, cMax]); 
  
end
pause(3);

%% save individual slice profiles for publication
close all;
figure(2); 
for i=size(ImgsPD,4):-1:1
    % extract slice
    Slc = rot90(squeeze(ImgsPD(xMin:xMax,yMin:yMax,TRASlc,i)));    
    SlcDM = rot90(squeeze(ImgsPDDM(xMin:xMax,yMin:yMax,TRASlc,i)));    
    
    % extract profiles
    Profile = Slc(xStart:xEnd, yStart:yEnd);   
    ProfileDM = SlcDM(xStart:xEnd, yStart:yEnd);   
    
    % plot profile
    plot(1:length(ProfileDM), ProfileDM','-c', 'LineWidth',10);
    hold on;
    plot(1:length(Profile), Profile','-r', 'LineWidth',7.5);
    hold off;
    
    % adjust axis etc.
    h=gca;
    h.XLim = ([-2, length(xStart:xEnd)+2]);
    h.YLim = ([-50, 450]);
    h.TickLength = [0,0];
        % to enable black background
    h.Color = [0 ,0, 0];
    set(gcf,'inverthardcopy','off'); 
        % grid
    h.XGrid = 'Off'; h.YGrid = 'On';
    h.GridColor = [1 1 1];
    h.YTick = 0:75:450;
    h.LineWidth = 5;
    % add legend
    if i == 1       
        legend({'masked', 'unmasked'},'FontSize',18,'FontWeight','bold',...
            'TextColor', 'white');
        legend('boxoff');
        legend off
    end
    
    % set filename
    Fn = [PathSave, 'Profile_PDBW',BWsPD{i},'.tiff'];
    
    % save
    set(gca(), 'LooseInset', get(gca(), 'TightInset'));
    print(Fn,'-dtiffn',['-r0']);   
    
end
