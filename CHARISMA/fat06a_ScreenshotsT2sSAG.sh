#!/bin/bash

# Folders
PathCoReg=${PWD}/02_CoReg/
PathDeMasked=${PWD}/04_Demasked/
PathOut=${PWD}/05_Output/

# Options
declare Opt=":grayscale=0,650 -slice 120 1 1 -viewport sagittal"


# list of all Vols (Co-Reg)
declare -a Vols=("greT2s1mmiPat2BW100_N4_Warped"
				"greT2s1mmiPat2BW200_N4_Warped"
				"greT2s1mmiPat2BW300_N4_Warped"
				"greT2s1mmiPat2BW390_N4_Warped"
				"greT2s1mmiPat2BW980_N4_Warped")

# Create and Save Screenshots
for i in "${Vols[@]}"
do
	freeview ${PathCoReg}${i}.nii.gz${Opt}  -ss ${PathOut}${i}.png 1
done

### Now for the demasked volumes again
# list of all Vols 
declare -a Vols=("T2sBW100_DeMasked"
				"T2sBW200_DeMasked"
				"T2sBW300_DeMasked"
				"T2sBW390_DeMasked"
				"T2sBW980_DeMasked")

# Create and Save Screenshots (Rigid)
for i in "${Vols[@]}"
do
	freeview ${PathDeMasked}${i}.nii${Opt}  -ss ${PathOut}${i}.png 1
done