clear; close all; clc;

%% init
% add nii handling
addpath('/home/hmattern/MATLAB/3rdParty/NIfTI_20140122/');

%PathImg = '/home/hmattern/Sandbox/CHARISMA/02_CoReg/';
PathImg = './02_CoReg/';
PathMask = './03_ilastik/';
PathDeMasked = './04_Demasked/';

BWsT2s = {'100', '200', '300', '390', '980'};
BWsPD = {'200', '980'};


%% load data T2s
ImgsT2s = zeros(224, 256, 96, length(BWsT2s));
MapsT2s = ImgsT2s;

for i=1:length(BWsT2s)
    Data = load_untouch_nii([PathImg, 'greT2s1mmiPat2BW', BWsT2s{i}, '_N4_Warped.nii.gz']);
    ImgsT2s(:,:,:,i) = Data.img;    
    
    % load also probability maps as h5 (use <h5disp(File, '/')> to get info
    Datah5 = h5read([PathMask, 'greT2s1mmiPat2BW', BWsT2s{i}, '_N4_Warped-data_Probabilities.h5'],'/exported_data');
    MapsT2s(:,:,:,i) = squeeze(Datah5(1,:,:,:)); % first label is oil mask
    % sometimes we have also a mask_water label (5th entry/label)
    if length(Datah5(:,1)) == 5
        MapsT2s(:,:,:,i) = MapsT2s(:,:,:,i) + squeeze(Datah5(5,:,:,:));
    end
    % also add the air label to the map to clear the background (improves
    % 3D rendering)
    MapsT2s(:,:,:,i) = MapsT2s(:,:,:,i) + squeeze(Datah5(2,:,:,:));
    
end


%% load data PD
ImgsPD = zeros(224, 256, 96, length(BWsPD));
MapsPD = ImgsPD;

for i=1:length(BWsPD)
    Data = load_untouch_nii([PathImg, 'grePD1mmiPat2BW', BWsPD{i}, 'noFlowComp_N4_Warped.nii.gz']);
    ImgsPD(:,:,:,i) = Data.img;
        
    % load also probability maps as h5 (use <h5disp(File, '/')> to get info
    Datah5 = h5read([PathMask, 'grePD1mmiPat2BW', BWsPD{i}, 'noFlowComp_N4_Warped-data_Probabilities.h5'],'/exported_data');
    MapsPD(:,:,:,i) = squeeze(Datah5(1,:,:,:)); % first label is oil mask
    % sometimes we have also a mask_water label (5th entry/label)
    if length(Datah5(:,1)) == 5
        MapsPD(:,:,:,i) = MapsPD(:,:,:,i) + squeeze(Datah5(5,:,:,:));
    end
    % also add the air label to the map to clear the background (improves
    % 3D rendering)
    MapsPD(:,:,:,i) = MapsPD(:,:,:,i) + squeeze(Datah5(2,:,:,:));
    
end


%% de-mask images T2s and save
ImgsT2sdm = zeros(size(ImgsT2s));

for i=1:size(ImgsT2s,4)    
    % demask
    ImgsT2sdm(:,:,:,i) = ImgsT2s(:,:,:,i) - ImgsT2s(:,:,:,i).*MapsT2s(:,:,:,i);                
    % no negative values
    ImgsT2sdm(ImgsT2sdm < 0) = 0;
    % save
    save_nii(make_nii(ImgsT2sdm(:,:,:,i)), [PathDeMasked, 'T2sBW',BWsT2s{i},'_DeMasked.nii'])
end

%% de-mask images PD and save
ImgsPDdm = zeros(size(ImgsPD));

for i=1:size(ImgsPD,4)    
    % demask
    ImgsPDdm(:,:,:,i) = ImgsPD(:,:,:,i) - ImgsPD(:,:,:,i).*MapsPD(:,:,:,i); 
    % no negative values
    ImgsPDdm(ImgsPDdm < 0) = 0;
    % save
    save_nii(make_nii(ImgsPDdm(:,:,:,i)), [PathDeMasked, 'PDBW',BWsPD{i},'_DeMasked.nii'])
end
