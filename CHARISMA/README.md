# CHARISMA

This is the code and data used for the MRM CHARISMA manuscript.
All scripts and folders are ordered to enable reproduction of all results.
To reduce the memory consumption of the data, the MR images, ilastik segmentations, and output figures are inculded, intermediate results (N4, CoReg, Demasked) are omitted.

## Requirements and Tools:
The following programs have been used in this work:
- ANTs (N4, co-registration)
- ilastik (segmentation)
- MATLAB (demasking, plotting of line profiles)
- ImageJ (3D rendering, nii-to-h5 conversion)
- freeview (screenshots)

Alternatively, other tools with similar functionality could be used.
The final figures in the manuscript were created with Inkscape using the MATLAB and freeview output.

## Additional Notes:
- "fat" prefix for files required for MATLAB naming compatibility
- for nii-handling in MATLAB the package [Tools for NIfTI and ANALYZE image](https://de.mathworks.com/matlabcentral/fileexchange/8797-tools-for-nifti-and-analyze-image) can be used; the package is NOT included in this project.
- everything tested on a linux system; path for nii-handling has to be adapted

