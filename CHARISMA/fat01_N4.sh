#!/bin/bash

# Folders
InPath=${PWD}/00_ImageData/
OutPath=${PWD}/01_N4/

# list of all T2s Vol with Mask
declare -a VolT2s=("greT2s1mmiPat2BW100" 
	 "greT2s1mmiPat2BW200"
	 "greT2s1mmiPat2BW300"
	 "greT2s1mmiPat2BW390"
	 "greT2s1mmiPat2BW980")

# N4 T2s with Mask
for i in "${VolT2s[@]}"
do

	echo
	echo ${i}

	N4BiasFieldCorrection -d 3 -i ${InPath}${i}.nii.gz -o ${OutPath}${i}_N4.nii.gz 

done

# N4 T2s without mask
N4BiasFieldCorrection -d 3 -i ${InPath}/greT2s1mmiPat2BW100NoMask.nii.gz -o ${OutPath}/greT2s1mmiPat2BW100NoMask_N4.nii.gz



# N4 for PD

# 200 with mask
N4BiasFieldCorrection -d 3 -i ${InPath}/grePD1mmiPat2BW200noFlowComp.nii.gz -o ${OutPath}/grePD1mmiPat2BW200noFlowComp_N4.nii.gz

# 980 with mask
N4BiasFieldCorrection -d 3 -i ${InPath}/grePD1mmiPat2BW980noFlowComp.nii.gz -o ${OutPath}/grePD1mmiPat2BW980noFlowComp_N4.nii.gz

# 980 without mask
N4BiasFieldCorrection -d 3 -i ${InPath}/grePD1mmiPat2BW980noFlowCompNoMask.nii.gz -o ${OutPath}/grePD1mmiPat2BW980noFlowCompNoMask_N4.nii.gz