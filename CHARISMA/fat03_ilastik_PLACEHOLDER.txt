This is a placeholder for the (semi)-manual segmentation done in ilastik. 
To import the nii-files to ilastik, a conversion to h5 has to be done (for example using the ilastik plugin for ImageJ/Fiji).
The segmentation used in the manuscript is already saved in the ilastik folder, so you can use or fine-tune them yourself.